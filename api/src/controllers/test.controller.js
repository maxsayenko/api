const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

const mockData = require('../dataMock/mock');
const pageSize = 50;

const getData = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  let data = mockData;
  if(options['page']) {
      const pageNumber = parseInt(options['page'], 10);
      const start = (pageNumber - 1) * pageSize;
      data = mockData.slice(start, start + pageSize);
  }
  res.send(data);
});

module.exports = {
    getData
};
